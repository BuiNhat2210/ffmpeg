package com.example.ffmpegmobile.add_text

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.arthenica.mobileffmpeg.FFprobe
import com.example.ffmpegmobile.R
import com.example.ffmpegmobile.databinding.ActivityAddTextBinding
import com.example.ffmpegmobile.dialog.LoadingDialog
import com.example.ffmpegmobile.utility.FFmpegUtils
import com.example.ffmpegmobile.utility.Utility
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import java.io.File
import java.io.FileOutputStream

class AddTextActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddTextBinding
    private val tagName = AddTextActivity::class.java.simpleName
    private lateinit var mVideoUri: String
    private var exoPlayer: ExoPlayer? = null
    private lateinit var loadingDialog: LoadingDialog

    private var widthScreen: Int = 0
    private var heightScreen: Int = 0
    private var offsetX: Float = 0F
    private var offsetY: Float = 0F
    private var isMoving = false
    private var currentX: Float = 0F
    private var currentY : Float = 0F
    private var mHeightVideo : Int = 0
    private var mWidthVideo : Int = 0
    private var locations = IntArray(2)
    private var textSize : Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddTextBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mVideoUri = intent.getStringExtra("video_uri")?:""
        initPlayer()
        createMediaItem(mVideoUri)
        initEvent()
        textSize = binding.edtText.textSize
        loadingDialog = LoadingDialog(this)
        Utility.screenSize(this){width, heigth ->
            widthScreen = width
            heightScreen = heigth
            Log.e(tagName, "size screen $widthScreen x $heightScreen textSize: $textSize")
        }
        Utility.videoSize(this, mVideoUri){ widthVideo, heigthVideo ->
            mWidthVideo = widthVideo
            mHeightVideo = heigthVideo
            Log.e(tagName, "origin video size: $mWidthVideo x $mHeightVideo")
        }

        Log.e(tagName, "size text: ${convertTextSize(textSize)}")
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initEvent(){
        binding.icBack.setOnClickListener {
            finish()
        }

        binding.btnAddText.setOnClickListener {
            binding.edtText.visibility = View.VISIBLE
        }

        binding.edtText.setOnTouchListener { view, motionEvent ->
            when(motionEvent.actionMasked){
                MotionEvent.ACTION_DOWN -> {
                    isMoving = true
                    offsetX = motionEvent.rawX - view.x
                    offsetY = motionEvent.rawY - view.y
                }
                MotionEvent.ACTION_MOVE -> {
                    if(isMoving){
                        val newX = motionEvent.rawX - offsetX
                        val newY = motionEvent.rawY - offsetY
                        binding.edtText.x = newX
                        binding.edtText.y = newY

                        currentX = (newX * mWidthVideo) / widthScreen
                        currentY = (newY * mHeightVideo) / heightScreen
                        Log.e(tagName, "cordinate: $currentX/$currentY")

                    }
                }
                MotionEvent.ACTION_UP -> {
                    isMoving = false
                }
            }
            false
        }

        binding.btnConfirm.setOnClickListener {
            val content = binding.edtText.text.toString()
            if (content.isEmpty()){
                Utility.toast(this, getString(R.string.not_enter_content))
            } else {
                loadingDialog.show()
                val filePath = getFontAssest().absolutePath
                FFmpegUtils.addTextToVideo(
                    context = this@AddTextActivity,
                    videoUri = mVideoUri,
                    content = content,
                    fontPath = filePath,
                    x = "$currentX",
                    y = "$currentY",
                    size = "${convertTextSize(textSize)}",
                    color = "#FF5252"
                ){ outputPath, returnCode ->
                    Log.e(tagName, "input path: $mVideoUri \noutputPath: $outputPath")
                    loadingDialog.dismiss()
                    Log.e(tagName, "return code: $returnCode")
                    Utility.toast(this, "return code: $returnCode")
                    binding.edtText.visibility = View.GONE
                    createMediaItem(outputPath)
                }
            }
        }
    }

    private fun initPlayer(){
        if (exoPlayer == null)
            exoPlayer = ExoPlayer.Builder(this).build()
        binding.player.player = exoPlayer
        exoPlayer?.addListener(object : Player.Listener{
            override fun onPlaybackStateChanged(playbackState: Int) {
                super.onPlaybackStateChanged(playbackState)
                when(playbackState){
                    Player.STATE_BUFFERING -> {
                        Log.e(tagName, "video in buffer")
                    }
                    Player.STATE_READY -> {
                        exoPlayer?.play()
                    }
                    Player.STATE_ENDED -> {
                        exoPlayer?.seekTo(0)
                    }
                    else -> {}
                }
            }
        })
    }

    private fun createMediaItem(uri: String){
        exoPlayer?.clearMediaItems()
        val mediaItem = MediaItem.fromUri(uri)
        exoPlayer?.addMediaItem(mediaItem)
        exoPlayer?.prepare()
    }

    override fun onPause() {
        super.onPause()
        exoPlayer?.pause()
    }

    override fun onStop() {
        super.onStop()
        exoPlayer?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer?.release()
    }

    private fun getFontAssest(): File{
        val assetManager = assets
        val fontName = "inter.ttf"
        val inputStream = assetManager.open(fontName)

        val cacheDir = cacheDir
        val fontFile = File(cacheDir, fontName)
        inputStream.use { input ->
            FileOutputStream(fontFile).use { output ->
                input.copyTo(output)
            }
        }

        if(fontFile.exists()){
            Log.e("check", "create file success")
        } else
            Log.e("check", "file not exits")
        Log.e("check", "url file: ${fontFile.absolutePath}")

        return fontFile
    }

    private fun convertTextSize(sp: Float): Int{
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            sp.toFloat(),
            resources.displayMetrics
        ).toInt()
    }
}