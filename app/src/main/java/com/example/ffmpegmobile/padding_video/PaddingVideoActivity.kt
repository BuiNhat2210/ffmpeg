package com.example.ffmpegmobile.padding_video

import android.R.attr.maxHeight
import android.R.attr.maxWidth
import android.R.attr.tag
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.graphics.Rect
import android.media.MediaMetadataRetriever
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowInsets
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.arthenica.mobileffmpeg.FFprobe
import com.example.ffmpegmobile.add_text.AddTextActivity
import com.example.ffmpegmobile.databinding.ActivityPaddingVideoBinding
import com.example.ffmpegmobile.dialog.LoadingDialog
import com.example.ffmpegmobile.utility.ExoPlayerUtils
import com.example.ffmpegmobile.utility.FFmpegUtils
import com.example.ffmpegmobile.utility.Utility
import com.google.android.exoplayer2.ExoPlayer


class PaddingVideoActivity : AppCompatActivity() {

    private var tagName = PaddingVideoActivity::class.java.simpleName
    private lateinit var binding : ActivityPaddingVideoBinding
    private var mVideoUri: String = ""
    private var exoPlayer: ExoPlayer? = null

    private var widthScreen: Int = 0
    private var heightScreen : Int = 0
    private var videoWidth: Int = 0
    private var videoHeight: Int = 0

    private var mOutputPath : String? = null

    private lateinit var loadingDialog : LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaddingVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mVideoUri = intent.getStringExtra("video_uri")?:""
        Log.e(tagName, "video uri: $mVideoUri")

        exoPlayer = ExoPlayer.Builder(this).build()
        ExoPlayerUtils.initExoPlayer(exoPlayer, binding.player)
        ExoPlayerUtils.createMediaItem(exoPlayer, mVideoUri)

        loadingDialog = LoadingDialog(this)

        getSizeVideo(mVideoUri)
        getScreenSize()
        initEvent()

        val mediaInformation = FFprobe.getMediaInformation(mVideoUri)
        val videoStream = mediaInformation.streams.firstOrNull { it.type == "video" }

        val videoWidth = videoStream?.width ?: 0
        val videoHeight = videoStream?.height ?: 0
        val videoAspectRatio = videoStream?.displayAspectRatio ?: "1:1"
        Log.e(tagName, "$videoWidth x $videoHeight x $videoAspectRatio")

    }

    private fun initEvent(){
        binding.btnPaddingVideo.setOnClickListener {
            loadingDialog.show()
            FFmpegUtils.makeVideoFullScreen(
                this,
                mVideoUri,
                widthScreen = widthScreen,
                heightScreen = heightScreen,
            ){outputPath, returnCode ->
                loadingDialog.dismiss()
                Log.e(tagName, "return code: $returnCode")
                ExoPlayerUtils.createMediaItem(exoPlayer, outputPath)
                mOutputPath = outputPath
                getSizeVideo(outputPath)
            }
        }

        binding.btnGoToAddText.setOnClickListener {
            if(mOutputPath == null)
                Utility.toast(this, "Bạn phải padding video trước")
            Intent(this, AddTextActivity::class.java).also {
                it.putExtra("video_uri", mOutputPath)
                startActivity(it)
            }
        }
    }

    private fun getSizeVideo(uri: String){
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(this, uri.toUri())
        videoWidth = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH)?.toInt()?:0
        videoHeight = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)?.toInt()?:0
        Log.e(tagName, "width Video: $videoWidth height Video: $videoHeight")
        retriever.release()
    }

    private fun getScreenSize(){
        val displayMetrics = resources.displayMetrics
        widthScreen = displayMetrics.widthPixels
        heightScreen = displayMetrics.heightPixels
        Log.e(tagName, "width Screen: $widthScreen height screen: $heightScreen")
    }

    private fun getScreenSizeC2(): Pair<Int, Int> {
        val displayMetrics = DisplayMetrics()
        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val windowMetrics = windowManager.currentWindowMetrics
            val insets = windowMetrics.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
            displayMetrics.widthPixels = windowMetrics.bounds.width() - insets.left - insets.right
            displayMetrics.heightPixels = windowMetrics.bounds.height() - insets.top - insets.bottom
        } else {
            val display = windowManager.defaultDisplay
            display.getMetrics(displayMetrics)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                val realMetrics = DisplayMetrics()
                display.getRealMetrics(realMetrics)
                displayMetrics.widthPixels = realMetrics.widthPixels
                displayMetrics.heightPixels = realMetrics.heightPixels
            }
        }

        Log.e(tagName, "${displayMetrics.heightPixels}")
        return Pair(displayMetrics.widthPixels, displayMetrics.heightPixels)
    }

    private fun getScreenSizeC3(){
        val rect = Rect()
        val window = window ?: return

        window.decorView.getWindowVisibleDisplayFrame(rect)

        val statusBarHeight = rect.top

        val navigationBarHeight = window.decorView.height - rect.bottom

        val contentHeight = window.decorView.height - statusBarHeight - navigationBarHeight

        Log.e(tagName, "size status: $statusBarHeight size navigation: $navigationBarHeight")

        Log.e(tagName, "Content Height: $contentHeight")
    }


    override fun onPause() {
        super.onPause()
        ExoPlayerUtils.pauseExoPlayer(exoPlayer)
    }

    override fun onStop() {
        super.onStop()
        ExoPlayerUtils.stopExoPlayer(exoPlayer)
    }

    override fun onDestroy() {
        super.onDestroy()
        ExoPlayerUtils.releasePlayer(exoPlayer)
    }

}