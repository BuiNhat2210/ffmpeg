package com.example.ffmpegmobile.broadcast

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import com.example.ffmpegmobile.download_mp3.DownloadMp3Activity

class MyDownloadBroadcast : BroadcastReceiver() {
    @SuppressLint("Range")
    override fun onReceive(context: Context?, intent: Intent?) {
        val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)?:-1
        if(id != -1L){
            val downloadManager = context?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val query = DownloadManager.Query().setFilterById(id)
            val cursor = downloadManager.query(query)
            if (cursor.moveToFirst()) {
                val status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                if (status == DownloadManager.STATUS_SUCCESSFUL) {
                    val uriString = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
                    var downloadedFilePath = Uri.parse(uriString).path
                    Log.e("check", "path audio file: $downloadedFilePath")
                    Intent(context, DownloadMp3Activity::class.java).also {
                        it.putExtra("result", downloadedFilePath)
                        context.startActivity(it)
                    }
                }
            }
            cursor.close()
        }
    }
}