package com.example.ffmpegmobile.save_file

import android.content.Context
import android.content.ContextWrapper
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.ffmpegmobile.R
import com.example.ffmpegmobile.constants.Constants
import com.example.ffmpegmobile.databinding.ActivitySaveFileBinding
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import java.io.File
import java.io.FileInputStream

class SaveFileActivity : AppCompatActivity(), Player.Listener {

    private lateinit var binding : ActivitySaveFileBinding
    private var mOutputPath : String? = null
    private var exoPlayer: ExoPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySaveFileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initPlayer()

        mOutputPath = intent.getStringExtra(Constants.Intent.OUTPUT_PATH)
        binding.btnSaveFile.setOnClickListener {
            mOutputPath?.let {
                val file = saveFileVideo(it)
                Log.e("check", "create file success: ${file.absolutePath}")
                createMediaItem(file.absolutePath)
            }
        }
    }

    private fun saveFileVideo(url: String) : File{
        val inputStream = FileInputStream(File(url))
        val cw = ContextWrapper(this)
        val dir = cw.getDir("merge_video", Context.MODE_PRIVATE)
        val file = File(dir, "output.mp4")
        val outputStream = file.outputStream()
        inputStream.copyTo(outputStream)
        inputStream.close()
        outputStream.close()
        return file
    }

    private fun initPlayer(){
        exoPlayer = ExoPlayer.Builder(this).build()
        binding.player.player = exoPlayer
        exoPlayer?.addListener(object : Player.Listener{
            override fun onPlaybackStateChanged(playbackState: Int) {
                super.onPlaybackStateChanged(playbackState)
                when(playbackState){
                    Player.STATE_BUFFERING -> {
                        Log.e("check", "buffer")
                    }
                    Player.STATE_READY -> {
                        exoPlayer?.play()
                    }
                    Player.STATE_ENDED -> {
                        exoPlayer?.seekTo(0)
                    }
                }
            }
        })
    }

    private fun createMediaItem(url: String){
        exoPlayer?.clearMediaItems()
        val mediaItem = MediaItem.fromUri(url)
        exoPlayer?.addMediaItem(mediaItem)
        exoPlayer?.prepare()
    }

    override fun onPause() {
        super.onPause()
        exoPlayer?.pause()
    }

    override fun onStop() {
        super.onStop()
        exoPlayer?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer?.release()
    }
}