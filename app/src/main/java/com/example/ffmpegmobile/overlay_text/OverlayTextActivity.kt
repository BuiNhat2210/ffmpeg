package com.example.ffmpegmobile.overlay_text

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContracts
import com.example.ffmpegmobile.R
import com.example.ffmpegmobile.constants.Constants
import com.example.ffmpegmobile.databinding.ActivityOverlayTextBinding
import com.example.ffmpegmobile.utility.ExoPlayerUtils
import com.example.ffmpegmobile.utility.Utility
import com.google.android.exoplayer2.ExoPlayer
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class OverlayTextActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOverlayTextBinding
    private var originVideoFile : File? = null
    private var exoPlayer: ExoPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOverlayTextBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initEvent()
        exoPlayer = ExoPlayer.Builder(this).build()
        ExoPlayerUtils.initExoPlayer(exoPlayer, binding.player)
    }

    private fun initEvent(){
        binding.btnSelectVideo.setOnClickListener {
            checkPermission()
        }
    }

    private fun checkPermission(){
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener{
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if(report!!.areAllPermissionsGranted()){
                        selectVideo()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?,
                ) {
                    token?.continuePermissionRequest()
                }
            }).onSameThread().check()
    }

    private fun selectVideo(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        launcher.launch(intent)
    }

    private val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if(it.resultCode == Activity.RESULT_OK){
            val videoUri = it.data?.data
            videoUri?.let { uri ->
                originVideoFile = saveVideoFile(uri)
                ExoPlayerUtils.createMediaItem(exoPlayer, originVideoFile!!.absoluteFile.toString())
            }
        }
    }

    private fun saveVideoFile(videoUri: Uri): File{
        val ips = contentResolver.openInputStream(videoUri)
        val dir = getExternalFilesDir(null)
        val file = File(dir, "origin_overlay_video.mp4")
        val ops = FileOutputStream(file)
        ips?.copyTo(ops)
        ips?.close()
        ops.close()
        return file
    }

    override fun onPause() {
        super.onPause()
        ExoPlayerUtils.pauseExoPlayer(exoPlayer)
    }

    override fun onStop() {
        super.onStop()
        ExoPlayerUtils.stopExoPlayer(exoPlayer)
    }

    override fun onDestroy() {
        super.onDestroy()
        ExoPlayerUtils.releasePlayer(exoPlayer)
    }

}