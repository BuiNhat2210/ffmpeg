import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.graphics.SurfaceTexture
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.Surface
import android.view.TextureView
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import java.io.FileOutputStream

class OverlayVideoHelper(
    private val context: Context,
    private val videoUri: Uri,
    private val outputPath: String
) {
    private var player: SimpleExoPlayer? = null
    private var overlayText: String = ""
    private var overlayPosition: Point? = null
    private var overlayTextColor: Int = Color.WHITE
    private var overlayTextSize: Float = 40f

    fun setOverlayText(text: String) {
        overlayText = text
    }

    fun setOverlayPosition(position: Point) {
        overlayPosition = position
    }

    fun setOverlayTextColor(color: Int) {
        overlayTextColor = color
    }

    fun setOverlayTextSize(size: Float) {
        overlayTextSize = size
    }

    fun overlayTextOnVideo() {
        val dataSourceFactory = DefaultDataSourceFactory(
            context,
            Util.getUserAgent(context, context.packageName)
        )
        val mediaItem = MediaItem.fromUri(videoUri)
        player?.addMediaItem(mediaItem)
        player?.playWhenReady = true

        val textureView = TextureView(context)
        textureView.surfaceTextureListener = object : TextureView.SurfaceTextureListener {
            override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
                val surface = Surface(surface)
                player!!.setVideoSurface(surface)
                addTextOverlay(textureView)
            }

            override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {}

            override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
                return true
            }

            override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}
        }
    }


    private fun addTextOverlay(textureView: TextureView) {
        val frameBitmap = textureView.bitmap
        val mutableBitmap = frameBitmap!!.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(mutableBitmap)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = overlayTextColor
        paint.textSize = overlayTextSize
        paint.textAlign = Paint.Align.LEFT

        val x = overlayPosition?.x ?: 0
        val y = overlayPosition?.y ?: 0

        canvas.drawText(overlayText, x.toFloat(), y.toFloat(), paint)

        val outputStream = FileOutputStream(outputPath)
        mutableBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        outputStream.close()

        player?.stop()
        player?.release()
        player = null

        Handler(Looper.getMainLooper()).post {
            // Handle the processed video here
        }
    }

    fun release() {
        player?.stop()
        player?.release()
        player = null
    }
}
