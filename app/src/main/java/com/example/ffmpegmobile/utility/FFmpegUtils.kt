package com.example.ffmpegmobile.utility

import android.content.Context
import android.os.Environment
import com.arthenica.mobileffmpeg.FFmpeg
import java.io.File


object FFmpegUtils {

    fun mergeVideo(
        context: Context,
        videoUri: String,
        audioUri: String,
        listener: (outputPath: String, returnCode: Int) -> Unit,
    ){

        val fileName = Utility.getCurrentTime()
        val externalDir = context.getExternalFilesDir(null)
        val file = File(externalDir, "temp_video${fileName}.mp4")
        val outputPath = file.absolutePath

        val cmd = arrayOf(
            "-i",
            videoUri,
            "-stream_loop",
            "-1",
            "-i",
            audioUri,
            "-c:v",
            "copy",
            "-c:a",
            "aac",
            "-map",
            "0:v:0",
            "-map",
            "1:a:0",
            "-shortest",
            outputPath
        )

        FFmpeg.executeAsync(cmd
        ) { _, returnCode ->
           listener(outputPath, returnCode)
        }
    }

    fun addTextToVideo(
        context: Context,
        videoUri: String,
        content: String?,
        fontPath: String,
        x: String,
        y: String,
        size: String,
        color: String,
        listener: (outputPath: String, returnCode: Int) -> Unit,
    ){
        val fileName = Utility.getCurrentTime()
        val externalDir = context.getExternalFilesDir(null)
        val file = File(externalDir, "temp_video${fileName}.mp4")
        val outputPath = file.absolutePath

        val cmd =  arrayOf(
            "-i", videoUri,
            "-vf", "drawtext=text=$content:x=$x:y=$y:fontsize=$size:fontcolor=$color:fontfile=$fontPath",
            "-c:a", "copy",
            "-b:v", "3M",
            outputPath
        )

        FFmpeg.executeAsync(cmd
        ) { _, returnCode ->
            listener(outputPath, returnCode)
        }
    }


    fun makeVideoFullScreen(
        context: Context,
        inputVideoPath: String,
        widthScreen: Int,
        heightScreen: Int,
        listener: (outputPath: String, returnCode: Int) -> Unit,
    ){
        val fileName = Utility.getCurrentTime()
        val externalDir = context.getExternalFilesDir(null)
        val file = File(externalDir, "temp_video${fileName}.mp4")
        val outputPath = file.absolutePath

        var targetHeight = (80 * heightScreen) / 100 // 80 % height of screen

        val cmd = arrayOf(
            "-i",
            inputVideoPath,
            "-vf",
            "pad=$widthScreen:$targetHeight:(ow-iw)/2:(oh-ih)/2",
            outputPath
        )

        FFmpeg.executeAsync(cmd
        ) { _, returnCode ->
            listener(outputPath, returnCode)
        }
    }

    fun overlayImage(
        context: Context,
        inputPath: String,
        imagePath: String,
        listener: (outputPath: String, returnCode: Int) -> Unit,
    ){
        val fileName = Utility.getCurrentTime()
        val externalDir = context.getExternalFilesDir(null)
        val file = File(externalDir, "temp_video${fileName}.mp4")
        val outputPath = file.absolutePath

//        val cmd = arrayOf(
//            "-i",
//            inputPath,
//            "-i",
//            imagePath,
//            "-filter_complex",
//            "[0:v][1:v]overlay=x=10:y=10:enable='1'",
//            "-c:a",
//            "copy",
//            outputPath
//        )
        val cmd = arrayOf(
            "-i",
            inputPath,
            "-i",
            imagePath,
            "-filter_complex",
            "[0:v][1:v]overlay=x=(W-w)/2:y=(H-h)/2:enable='1'",
            "-c:a",
            "copy",
            outputPath
        )


        FFmpeg.executeAsync(cmd
        ) { _, returnCode ->
            listener(outputPath, returnCode)
        }
    }

}