package com.example.ffmpegmobile.utility

import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerView

object ExoPlayerUtils {
    fun initExoPlayer(exoPlayer: ExoPlayer?, playerview: PlayerView){
        playerview.player = exoPlayer
        exoPlayer?.addListener(object : Player.Listener{
            override fun onPlaybackStateChanged(playbackState: Int) {
                super.onPlaybackStateChanged(playbackState)
                if(playbackState == Player.STATE_ENDED){
                    exoPlayer.seekTo(0)
                }
                else if(playbackState == Player.STATE_READY){
                    exoPlayer.play()
                }
            }
        })
    }

    fun createMediaItem(exoPlayer: ExoPlayer? ,videoUri: String){
        exoPlayer?.clearMediaItems()
        val mediaItem = MediaItem.fromUri(videoUri)
        exoPlayer?.addMediaItem(mediaItem)
        exoPlayer?.prepare()
        exoPlayer?.play()
    }

    fun pauseExoPlayer(exoPlayer: ExoPlayer?){
        exoPlayer?.pause()
    }

    fun stopExoPlayer(exoPlayer: ExoPlayer?){
        exoPlayer?.stop()
    }

    fun releasePlayer(exoPlayer: ExoPlayer?){
        exoPlayer?.release()
    }
}