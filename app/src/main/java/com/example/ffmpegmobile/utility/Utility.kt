package com.example.ffmpegmobile.utility

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.net.toUri
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

object Utility {
    fun toast(context: Context, msg: String){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun getCurrentTime(fotmat: String = "HH_mm_ss"): String{
        val myCalendar = Calendar.getInstance()
        val sdf = SimpleDateFormat(fotmat, Locale.getDefault())
        return sdf.format(myCalendar.time)
    }

    @SuppressLint("Range")
    private fun getPhysicalPath(context: Context, videoUri: Uri) : String?{
        val columnData = arrayOf(MediaStore.MediaColumns.DATA)
        val cursor = context.contentResolver.query(
            videoUri,
            columnData,
            null, null, null
        )
        if(cursor != null){
            cursor.moveToFirst()
            val filePath = cursor.getString(
                cursor.getColumnIndex(columnData[0])
            )
            cursor.close()
            return filePath
        }
        return null
    }

    fun screenSize(context: Context, listener: (width: Int, heigth: Int) -> Unit){
        val displayMetrics = context.resources.displayMetrics
        val widthScreen = displayMetrics.widthPixels
        val heightScreen = displayMetrics.heightPixels
        listener(widthScreen, heightScreen)
    }

    fun videoSize(context: Context ,videoUri: String, listener: (widthVideo: Int, heigthVideo: Int) -> Unit){
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context, videoUri.toUri())
        val withVideo = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH)?.toInt()?:0
        val heightVideo = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)?.toInt()?:0
        listener(withVideo, heightVideo)

    }
}