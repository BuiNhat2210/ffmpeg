package com.example.ffmpegmobile.download_mp3

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.ffmpegmobile.MainActivity
import com.example.ffmpegmobile.databinding.ActivityDownloadHttpBinding
import com.example.ffmpegmobile.dialog.LoadingDialog
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class DownloadHttpActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDownloadHttpBinding
    private var mFilePath: String? = null
    private lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDownloadHttpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadingDialog = LoadingDialog(this)

        binding.btnDownload.setOnClickListener {
            checkPermission()
        }

        binding.btnDeleteFile.setOnClickListener {
            if(mFilePath == null){
                Toast.makeText(this, "File not exits or File is downloading", Toast.LENGTH_SHORT).show()
            } else{
                val file = File(mFilePath!!)
                if(file.exists()){
                    val isDelete = file.delete()
                    if(isDelete){
                        Log.e("check", "Delete file success")
                    } else
                        Log.e("check", "Delete failed")
                } else
                    Log.e("check", "file not exits")
            }
        }

        binding.btnBack.setOnClickListener {
            Intent(this, MainActivity::class.java).also {
                it.putExtra("url_mp3", mFilePath)
                it.putExtra("is_download_mp3", true)
                startActivity(it)
            }
        }
    }

    private fun checkPermission(){
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener{
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if(report!!.areAllPermissionsGranted()){
                        Log.e("check", "permission is allowed")
                        downloadMp3(
                            "https://storage.googleapis.com/exoplayer-test-media-0/play.mp3",
                            "test_http_file.mp3"
                        )
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?,
                ) {
                    token?.continuePermissionRequest()
                }
            }).onSameThread().check()
    }

    private fun downloadMp3(mp3Url: String?, fileName: String?) {
        loadingDialog.show()
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val url = URL(mp3Url)
                val connection = url.openConnection() as HttpURLConnection
                connection.connect()

                // Kiểm tra response code
                val responseCode = connection.responseCode
                Log.e("check", "response code: $responseCode")
                if (responseCode == HttpURLConnection.HTTP_OK) {

                    val externalDir = getExternalFilesDir(null)

                    // Tạo file trong thư mục với tên fileName
                    val file = File(externalDir, fileName)
                    val outputStream = FileOutputStream(file)

                    // Đọc dữ liệu từ InputStream và ghi vào file
                    val inputStream = connection.inputStream
                    inputStream.copyTo(outputStream)

                    // Đóng luồng
                    inputStream.close()
                    outputStream.close()
                    mFilePath = file.absolutePath
                    Log.e("check", "path file mp3: $mFilePath")
                    withContext(Dispatchers.Main){
                        loadingDialog.hide()
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}