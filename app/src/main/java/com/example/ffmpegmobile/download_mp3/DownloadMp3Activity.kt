package com.example.ffmpegmobile.download_mp3

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.FileUtils
import android.util.Log
import com.example.ffmpegmobile.broadcast.MyDownloadBroadcast
import com.example.ffmpegmobile.databinding.ActivityDownloadMp3Binding
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class DownloadMp3Activity : AppCompatActivity() {

    private lateinit var binding: ActivityDownloadMp3Binding
    private var downloadId = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDownloadMp3Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnDownload.setOnClickListener {
            downloadMp3(
                "https://storage.googleapis.com/exoplayer-test-media-0/play.mp3",
                "play_test.mp3"
            )
        }

        binding.btnBack.setOnClickListener {
            finish()
        }

        binding.btnDelete.setOnClickListener {
            val path = Environment.getExternalStorageDirectory().path + "/Download/play_test.mp3"
            val file = File(path)
            if(file.exists()){
                val isDelete = file.delete()
                if(isDelete){
                    Log.e("check", "delete successful")
                } else
                    Log.e("check", "delete fail")
            } else{
                Log.e("check", "file not exits")
            }
        }

        val down = intent.getStringExtra("result")
        Log.e("check", "result: $down")
    }

    private fun downloadMp3(url: String, fileName: String){
        val request = DownloadManager.Request(Uri.parse(url))
            .setTitle(fileName)
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            .setAllowedOverMetered(true)
            .setAllowedOverRoaming(true)
            .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)

        val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadId = downloadManager.enqueue(request)
        val intent = Intent(this, MyDownloadBroadcast::class.java)
        intent.putExtra("test", downloadId)
        sendBroadcast(intent)
    }

    private val downloadCompleteReceiver = object : BroadcastReceiver() {
        @SuppressLint("Range")
        override fun onReceive(context: Context, intent: Intent) {
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (id == downloadId) {
                val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                val query = DownloadManager.Query().setFilterById(downloadId)
                val cursor = downloadManager.query(query)
                if (cursor.moveToFirst()) {
                    val status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        val uriString = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
                        var downloadedFilePath = Uri.parse(uriString).path
                        Log.e("check", "path audio file: $downloadedFilePath")
                        // Thực hiện các hoạt động khác với đường dẫn tệp đã tải về ở đây
                    }
                }
                cursor.close()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        registerReceiver(downloadCompleteReceiver, filter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(downloadCompleteReceiver)
    }
}