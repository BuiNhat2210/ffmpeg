package com.example.ffmpegmobile.overlay

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import com.example.ffmpegmobile.R
import com.example.ffmpegmobile.constants.Constants
import com.example.ffmpegmobile.databinding.ActivityOverlayImageBinding
import com.example.ffmpegmobile.databinding.ActivityOverlayTextBinding
import com.example.ffmpegmobile.dialog.LoadingDialog
import com.example.ffmpegmobile.utility.ExoPlayerUtils
import com.example.ffmpegmobile.utility.FFmpegUtils
import com.google.android.exoplayer2.ExoPlayer
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.FileOutputStream

class OverlayImageActivity : AppCompatActivity() {

    private lateinit var binding : ActivityOverlayImageBinding
    private var fileVideo : File? = null
    private var fileImage: File? = null
    private var exoPlayer : ExoPlayer? = null

    private var isShow = false
    private var tagName = OverlayImageActivity::class.java.simpleName
    private lateinit var loadingDialog : LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOverlayImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        exoPlayer = ExoPlayer.Builder(this).build()
        ExoPlayerUtils.initExoPlayer(exoPlayer, binding.player)
        loadingDialog = LoadingDialog(this)
        initEvent()
    }

    private fun initEvent(){
        binding.btnSelectVideo.setOnClickListener {
            checkPermission()
        }

        binding.btnCheckImage.setOnClickListener {
            isShow = !isShow
            if(isShow){
                binding.img.setImageURI(fileImage!!.absolutePath.toUri())
                binding.img.visibility = View.VISIBLE
            }
            else
                binding.img.visibility = View.GONE
        }

        binding.btnSelectImage.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            launcherImage.launch(intent)
        }

        binding.btnConfirm.setOnClickListener {
            if(fileImage == null || fileVideo == null)
                Toast.makeText(this, "Bạn phải chọn có file video và vile image", Toast.LENGTH_SHORT).show()
            else{
                loadingDialog.show()
                FFmpegUtils.overlayImage(
                    this,
                    fileVideo!!.absolutePath,
                    fileImage!!.absolutePath
                ){ outputPath, returnCode ->
                    loadingDialog.dismiss()
                    Log.e(tagName, "return code: $returnCode")
                    ExoPlayerUtils.createMediaItem(exoPlayer, outputPath)
                }
            }
        }
    }

    private fun checkPermission(){
        Dexter.withContext(this)
            .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener{
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    selectVideo()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: MutableList<PermissionRequest>?,
                    token: PermissionToken?,
                ) {
                    token?.continuePermissionRequest()
                }
            }).onSameThread().check()
    }

    private fun selectVideo(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        launcherVideo.launch(intent)
    }

    private var launcherVideo = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if(it.resultCode == Activity.RESULT_OK){
            val uri = it.data?.data
            uri?.let {
                fileVideo = saveTempFileInExternal(it)
                ExoPlayerUtils.createMediaItem(exoPlayer, fileVideo!!.absolutePath)
            }
        }
    }

    private var launcherImage = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if(it.resultCode == Activity.RESULT_OK){
            val imageUri = it.data?.data
            imageUri?.let {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                fileImage = saveFileImageInExternal(bitmap)
            }
        }
    }

    private fun saveTempFileInExternal(videoUri: Uri): File {
        val inputStream = contentResolver.openInputStream(videoUri)
        val externalDir = getExternalFilesDir(null)
        val file = File(externalDir, "temp_video.mp4")
        val outputStream = FileOutputStream(file)
        inputStream?.copyTo(outputStream)
        inputStream?.close()
        outputStream.close()
        return file
    }

    private fun saveFileImageInExternal(bitmap: Bitmap): File {
        val externalDir = getExternalFilesDir(null)
        val file = File(externalDir, "temp_image.png")
        val outputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        outputStream?.flush()
        outputStream.close()
        return file
    }


    override fun onPause() {
        super.onPause()
        ExoPlayerUtils.pauseExoPlayer(exoPlayer)
    }

    override fun onStop() {
        super.onStop()
        ExoPlayerUtils.stopExoPlayer(exoPlayer)
    }

    override fun onDestroy() {
        super.onDestroy()
        ExoPlayerUtils.releasePlayer(exoPlayer)
    }
}