package com.example.ffmpegmobile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.ffmpegmobile.add_text.AddTextActivity
import com.example.ffmpegmobile.constants.Constants
import com.example.ffmpegmobile.databinding.ActivityMainBinding
import com.example.ffmpegmobile.dialog.LoadingDialog
import com.example.ffmpegmobile.download_mp3.DownloadHttpActivity
import com.example.ffmpegmobile.overlay.OverlayImageActivity
import com.example.ffmpegmobile.padding_video.PaddingVideoActivity
import com.example.ffmpegmobile.save_file.SaveFileActivity
import com.example.ffmpegmobile.utility.FFmpegUtils
import com.example.ffmpegmobile.utility.Utility
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.FileOutputStream


class MainActivity : AppCompatActivity(), Player.Listener{

    private lateinit var binding : ActivityMainBinding
    private val tagName = MainActivity::class.java.simpleName

    private var exoPlayer: ExoPlayer? = null

    private var mVideoUri: String? = null
    private var mAudioUri : String? = null
    private var mOutputPath: String = ""
    private lateinit var loadingDialog: LoadingDialog
    private var isMerge = false

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadingDialog = LoadingDialog(this)

        val isDownloadMp3 = intent.getBooleanExtra("is_download_mp3", false)
        val linkDonwloadMp3 = intent.getStringExtra("url_mp3")
        if (isDownloadMp3){
            mAudioUri = linkDonwloadMp3
        }

        initPlayer()
        initEvent()
    }

    private fun initEvent(){
        // select video from grallery
        binding.btnSelectVideo.setOnClickListener {
            checkPermission()
        }

        // merge audio with video
        binding.btnMerge.setOnClickListener {
            if (mVideoUri == null || mAudioUri == null){
                Utility.toast(this, getString(R.string.select_both_file))
            } else{
                loadingDialog.show()
                FFmpegUtils.mergeVideo(this, mVideoUri!!, mAudioUri!!){ outputPath, returnCode ->
                    loadingDialog.dismiss()
                    mOutputPath = outputPath
                    Log.e(tagName, "return code: $returnCode")
                    if (returnCode == 0){
                        Utility.toast(this, getString(R.string.merge_success))
                        createMediaItem(outputPath)
                    }
                    else
                        Utility.toast(this, getString(R.string.merge_failed))
                }
            }
        }

        // select audio from device
        binding.btnSelectAudio.setOnClickListener {
            if (mVideoUri != null)
                selectAudio()
            else
                Utility.toast(this, getString(R.string.not_select_video))
        }

        // go to download screen
        binding.btnGoToDownload.setOnClickListener {
            val intent = Intent(this, DownloadHttpActivity::class.java)
            startActivity(intent)
        }

        // delete file output video after ffmpeg execute
        binding.btnDelete.setOnClickListener {
            val outputFile = File(mOutputPath)
            if (outputFile.exists()){
                val isDelete = outputFile.delete()
                if(isDelete){
                    Log.e(tagName, getString(R.string.delete_success))
                    Utility.toast(this, getString(R.string.delete_success))
                } else{
                    Utility.toast(this, getString(R.string.delete_failed))
                    Log.e(tagName, "can read: ${outputFile.canRead()} can write: ${outputFile.canWrite()}")
                }
            } else{
                Log.e(tagName, getString(R.string.file_not_exist))
                Utility.toast(this, getString(R.string.file_not_exist))
            }
        }

        // go to add text screen
        binding.btnAddText.setOnClickListener {
            if(mVideoUri == null)
                Utility.toast(this, getString(R.string.not_select_video))
            else{
                Intent(this, AddTextActivity::class.java).also {
                    it.putExtra("video_uri", mVideoUri)
                    startActivity(it)
                }
            }
        }

        // go to save file screen
        binding.btnSaveFile.setOnClickListener {
            Intent(this, SaveFileActivity::class.java).also {
                if(isMerge)
                    it.putExtra(Constants.Intent.OUTPUT_PATH, mOutputPath)
                else
                    it.putExtra(Constants.Intent.OUTPUT_PATH, mVideoUri)
                startActivity(it)
            }
        }

        // go to padding video screen
        binding.btnPaddingVideo.setOnClickListener {
            Intent(this, PaddingVideoActivity::class.java).also {
                it.putExtra("video_uri", mVideoUri)
                startActivity(it)
            }
        }

        binding.btnOverlay.setOnClickListener {
            val intent = Intent(this, OverlayImageActivity::class.java)
            startActivity(intent)
        }
    }

    private fun checkPermission(){
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener{
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if(report!!.areAllPermissionsGranted()){
                        selectVideo()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?,
                ) {
                    token?.continuePermissionRequest()
                }
            }).onSameThread().check()
    }

    private fun selectVideo(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        launcherVideo.launch(intent)
    }

    private val launcherVideo = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if(it.resultCode == Activity.RESULT_OK){
            val videoUri = it.data?.data
            val file = saveTempFileInExternal(videoUri!!)
            mVideoUri = file.absolutePath
            Log.e(tagName, "physical video uri: $mVideoUri")
            createMediaItem(mVideoUri!!)
        }
    }

    private fun selectAudio(){
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
        launcherMedia.launch(intent)
    }

    private val launcherMedia = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if (it.resultCode == Activity.RESULT_OK){
            val audioUri = it.data?.data
            val file = saveTempMp3FileInExternal(audioUri!!)
            mAudioUri = file.absolutePath
            Log.e(tagName, "physical audio path: $mAudioUri")
        }
    }

    /**
     * Exo player
     */
    private fun initPlayer(){
        if (exoPlayer == null)
            exoPlayer = ExoPlayer.Builder(this).build()
        binding.player.player = exoPlayer
        exoPlayer?.addListener(object : Player.Listener{
            override fun onPlaybackStateChanged(playbackState: Int) {
                super.onPlaybackStateChanged(playbackState)
                when(playbackState){
                    Player.STATE_BUFFERING -> {
                        Log.e(tagName, "video in buffer")
                    }
                    Player.STATE_READY -> {
                        exoPlayer?.play()
                    }
                    Player.STATE_ENDED -> {
                        exoPlayer?.seekTo(0)
                    }
                    else -> {}
                }
            }
        })
    }

    private fun createMediaItem(uri: String){
        exoPlayer?.clearMediaItems()
        val mediaItem = MediaItem.fromUri(uri)
        exoPlayer?.addMediaItem(mediaItem)
        exoPlayer?.prepare()
    }

    override fun onPause() {
        super.onPause()
        exoPlayer?.pause()
    }

    override fun onStop() {
        super.onStop()
        exoPlayer?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer?.release()
    }

    private fun saveTempFileInExternal(videoUri: Uri): File {
        val inputStream = contentResolver.openInputStream(videoUri)
        val externalDir = getExternalFilesDir(null)
        val file = File(externalDir, "temp_video.mp4")
        val outputStream = FileOutputStream(file)
        inputStream?.copyTo(outputStream)
        inputStream?.close()
        outputStream.close()
        return file
    }

    private fun saveTempMp3FileInExternal(videoUri: Uri): File {
        val inputStream = contentResolver.openInputStream(videoUri)
        val externalDir = getExternalFilesDir(null)
        val file = File(externalDir, "test.mp3")
        val outputStream = FileOutputStream(file)
        inputStream?.copyTo(outputStream)
        inputStream?.close()
        outputStream.close()
        return file
    }

    override fun onResume() {
        super.onResume()
        binding.tvAni.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_music))
    }
}